---
home: true
actionText: 🙂 Soon... 👋
actionLink: /SOON
features:
- title: Jay feature one
  details: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce lacinia eget dui eget mattis. Nullam sit amet erat nec ex pulvinar lacinia nec quis velit. Vivamus vitae nisi a sem egestas vehicula sit amet sed turpis.
- title: Jay feature two
  details: Morbi gravida ligula at imperdiet rhoncus. Ut sem odio, ultricies id venenatis eu, blandit non enim. Nam sed enim semper, venenatis nunc eu, porttitor leo.
- title: Jay feature one
  details: Etiam efficitur eros et tincidunt lobortis. Mauris facilisis ante in justo iaculis vestibulum. Cras euismod diam ut bibendum volutpat. Pellentesque elit lacus, lacinia vitae suscipit eu, varius sit amet ipsum.
footer: MIT Licensed | Copyright © 2018–2019 Philippe Charrière | a Bots.Garden production
---

# jay.gitlab.io

